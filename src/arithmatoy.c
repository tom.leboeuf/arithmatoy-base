#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

int checkChar(char *s) {
  const char *alldigits = get_all_digits();
  for (int i = strlen(s); i > 0; i--) {
    if (strchr(alldigits, s[i]) == NULL) {
      return 1;
    }
  }
  return 0;
}

const char *drop_leading_zeros(const char *number) {
/* char *drop_leading_zero(char *number) { */
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

long long int baseNToDec(const char *number, unsigned long long int base) {
    int len = strlen(number);
    int power = 1;
    int num = 0;

    for (int i = len - 1; i >= 0; i--) {
        if (get_digit_value(number[i]) >= base)
            return -1;
        num += get_digit_value(number[i]) * power;
        power = power * base;
    }
    return num;
}

char *decToBaseN(long long int dec, unsigned long long int base) {
    char *res;
    int i = 0;
    res = malloc(0);
    while (dec > 0) {
        res = realloc(res, sizeof(char) * (i + 1));
        res[i] = to_digit(dec%base);
        i++;
        dec /= base;
    }
    reverse(res);
    if (checkChar(res) == 1 || dec < 0) {
      return NULL;
    }
    if (strcmp(res, "") == 0) {
      char *newres = malloc(1 * sizeof(char));
      newres = "0";
      return newres;
    }
    return res;
}

int count_digit(int number, int base) {
   int count = 0;
   while(number != 0) {
      number = number / base;
      count++;
   }
   return count;
}

int get_digit_in_number(int number, int base, int which) {
    int digit;
    int i = 0;
    while(number){
        digit = number % base;
        number = number / base;
        if (i == which) {
            return digit;
        }
        i++;
    }
  return 0;
}


char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

    int i = 0;

    int lhssize;
    int rhssize;
    lhssize = strlen(lhs);
    rhssize = strlen(rhs);

    int retenue = 0;
    char *res = NULL;

    int tmp = 0;

    char *buffl;
    buffl = malloc(lhssize + 1);
    strcpy(buffl, lhs);
    reverse(buffl);
    char *buffr;
    buffr = malloc(rhssize + 1);
    strcpy(buffr, rhs);
    reverse(buffr);

    while (1 == 1) {

        tmp = 0;

        // cas normal
        if (i < lhssize && i < rhssize) {
            tmp = get_digit_value( buffl[i]) + get_digit_value(buffr[i]) + retenue;
            // cas si depassement de un
        } else if (i < lhssize && i >= rhssize) {
            tmp = get_digit_value(buffl[i]) + retenue;
            // cas si depassement de l' autre
        } else if (i >= lhssize && i < rhssize) {
            tmp = get_digit_value(buffr[i]) + retenue;
        }

        retenue = 0;
        if (tmp >= (int)base) {
            retenue = 1;
            tmp -= base;
        }


        res = realloc(res, sizeof(char) * i + 2);
        res[i] = to_digit(tmp);
        tmp = 0;

        if (i >= lhssize - 1 && i >= rhssize - 1) {
            if (retenue > 0) {
                res = realloc(res, sizeof(char) * (i + 2));
                res[i + 1] = to_digit(retenue);
                i++;
            }
            res = realloc(res, sizeof(char) * (i + 2));
            res[i + 1] = '\0';
            break;
        }
        i++;

    }
    return (void *)drop_leading_zeros((void *)reverse(res));
    /* return decToBaseN((baseNToDec(lhs, base) - baseNToDec(rhs, base)), base); */
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

    int lhssize;
    int rhssize;
    lhssize = strlen(lhs);
    rhssize = strlen(rhs);

    int retenue = 0;
    char *res = NULL;

    int tmp = 0;

    char *buffl;
    buffl = malloc(lhssize + 1);
    strcpy(buffl, lhs);
    reverse(buffl);
    char *buffr;
    buffr = malloc(rhssize + 1);
    strcpy(buffr, rhs);
    reverse(buffr);

    int i = 0;
    while (1 == 1) {

        tmp = 0;
        if (i < lhssize && i < rhssize) {
            tmp = get_digit_value( buffl[i]) - get_digit_value(buffr[i]) - retenue;
            // cas si depassement de un
        } else if (i < lhssize && i >= rhssize) {
            tmp = get_digit_value(buffl[i]) - retenue;
            // cas si depassement de l' autre
        } else if (i >= lhssize && i < rhssize) {
            tmp = get_digit_value(buffr[i]) - retenue;
        }

        if (i >= lhssize && i >= rhssize && retenue == 1) {
            return NULL;
        }
        retenue = 0;
        if (tmp < 0) {
            retenue = 1;
            tmp += base;
        }

        res = realloc(res, sizeof(char) * i + 2);
        res[i] = to_digit(tmp);

        tmp = 0;
        if (i >= lhssize && i >= rhssize) {
            if (retenue > 0) {
                return NULL;
                break;
            }
            res = realloc(res, sizeof(char) * (i + 2));
            res[i + 1] = '\0';
            break;
        }
        i++;
    }
    return (void *)drop_leading_zeros((void *)reverse(res));
  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
    /* return decToBaseN((baseNToDec(lhs, base) - baseNToDec(rhs, base)), base); */
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
      fprintf(stderr, "mul: entering function\n");
    }
    // Fill the function, the goal is to compute lhs * rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result

    // refaire ca
    /* char *res = "0"; */
    /* char *rest = (void *)lhs; */
    /* while (1) { */
    /*     res = arithmatoy_add(base, res, rhs); */
    /*     rest = arithmatoy_sub(base, rest, "1"); */
    /*     if (strcmp(rest, "0") == 0) { */
    /*         break; */
    /*     } */
    /* } */
    /* return res; */
    // end of refaire ca

    int lhssize;
    int rhssize;
    lhssize = strlen(lhs);
    rhssize = strlen(rhs);

    int retenue = 0;
    char *res = NULL;

    int tmp = 0;

    char *buffl;
    buffl = malloc(lhssize + 1);
    strcpy(buffl, lhs);
    reverse(buffl);
    char *buffr;
    buffr = malloc(rhssize + 1);
    strcpy(buffr, rhs);
    reverse(buffr);

    int i = 0;
    while (1 == 1) {

        tmp = 0;
        if (i < lhssize && i < rhssize) {
            tmp = get_digit_value( buffl[i]) - get_digit_value(buffr[i]) - retenue;
            // cas si depassement de un
        } else if (i < lhssize && i >= rhssize) {
            tmp = get_digit_value(buffl[i]) - retenue;
            // cas si depassement de l' autre
        } else if (i >= lhssize && i < rhssize) {
            tmp = get_digit_value(buffr[i]) - retenue;
        }

        // mettre tmp en dessous de la base
        while (tmp >= (int)base) {
          retenue++;
          tmp -= base;
        }

        // appliquer la base
        res = realloc(res, sizeof(char) * i + 2);
        res[i] = to_digit(tmp);
        tmp = 0;

        if (i >= lhssize - 1 && i >= rhssize - 1) {
            i++;
            for (int j = 0; j < count_digit(retenue, base); j++, i++) {
                res = realloc(res, sizeof(char) * i + 2);
                res[i] = to_digit(get_digit_in_number(retenue, base, count_digit(retenue, base) - j + 1));
            }
        }

        i++;
    }

    return res;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right

  // refaire ca

  /* const size_t length = strlen(str); */
  /* const size_t bound = length / 2; */
  /* for (size_t i = 0; i < bound; ++i) { */
  /*   char tmp = str[i]; */
  /*   const size_t mirror = length - i - 1; */
  /*   str[i] = str[mirror]; */
  /*   str[mirror] = tmp; */
  /* } */
  /* return str; */

  // end of refaire ca



}



void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
