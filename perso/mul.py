#!/usr/bin/env python3

def reverse(number: str) -> str:
    return ''.join(reversed(number))

def get_digit_value(digit: str) -> int:
    if digit in '0123456789':
        return ord(digit) - ord('0')
    if digit in 'abcdefghijklmnopqrstuvwxyz':
        return 10 + (ord(digit) - ord('a'))
    return -1

def get_all_digits():
    return "0123456789abcdefghijklmnopqrstuvwxyz"

def to_digit(value: int) -> str:
    if (value >= 36): # la valeur max des chars que l'ont peut avoir
        return 0;
    return get_all_digits()[value]

def mul_digit(base: int, product: str, mul: str) -> str:
    if len(mul) > 1:
        return ''

    buff = reverse(product)
    mulint: int = get_digit_value(mul)
    retenue = 0
    res = ''
    tmp = 0;

    i = 0
    while(True):
        tmp = 0
        if i > len(product) - 1 and retenue == 0:
            return reverse(res)
        if i > len(product) - 1 and retenue > 0:
            res += str(retenue)
            return res
        tmp = get_digit_value(buff[i]) * mulint + retenue
        retenue = 0
        if tmp >= base:
            retenue = int(tmp / base)
            tmp = int(tmp % base)
        res += to_digit(int(tmp))
        i += 1
    return reverse(res)

def pow_base(number: str, pownbr: int):
    number += '0' * (pownbr - 1)
    return number

def mul(base: int, lhs: str, rhs: str) -> str:
    pass
    # tmp = lhs
    # for i in range(len(rhs)):
    #     tmp = pow_base(tmp, i)
    #     tmp = mul_digit(base, tmp, reverse(rhs)[i])

    # return reverse(tmp)

print(mul(10, "12345", "12345"))

# print(pow_base("12345", 2))
