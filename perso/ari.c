#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

int get_digit_in_number(int number, int base, int which) {
    int digit;
    int i = 0;
    while(number){
        digit = number % base;
        number = number / base;
        if (i == which) {
            return digit;
        }
        i++;
    }
  return 0;
}

int count_digit(int number, int base) {
   int count = 0;
   while(number != 0) {
      number = number / base;
      count++;
   }
   return count;
}


char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
/* char *drop_leading_zero(char *number) { */
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}


char *mul_digit(unsigned int base, char *product, char mul) {
    int productsize;
    productsize = strlen(product);

    int retenue = 0;
    int tmp = 0;
    char *res = NULL;
    int mulint = get_digit_value(mul);

    char *buffproduct;
    buffproduct = malloc(productsize + 1);
    strcpy(buffproduct, product);
    reverse(buffproduct);

    int i = 0;
    while (1) {
        tmp = 0;
        if (i > productsize - 1 && retenue == 0) {
            reverse(res);
            return res;
        }
        if (i > productsize - 1 && retenue > 0) {
            res = realloc(res, sizeof(char) * i + 2);
            res[i] = to_digit(retenue);
            reverse(res);
            return res;
        }
        tmp = get_digit_value(buffproduct[i]) * mulint + retenue;
        retenue = 0;

        if (tmp > (int)base) {
            retenue = tmp / base;
            tmp = tmp % base;
        }
        res = realloc(res, sizeof(char) * i + 1);
        res[i] = to_digit(tmp);

        i++;
    }
    reverse(res);
    return res;
}

char *pow_base(char *number, int pow) {
    char zero = '0';
    for (int i = 0; i < pow; i ++) {
        number = realloc(number, sizeof(number) + 1);
        strcat(number, &zero);
    }
    return number;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    int lhssize;
    int rhssize;
    lhssize = strlen(lhs);
    rhssize = strlen(rhs);

    int retenue = 0;
    char *res = NULL;

    int tmp = 0;

    char *buffl;
    buffl = malloc(lhssize + 1);
    strcpy(buffl, lhs);
    reverse(buffl);
    char *buffr;
    buffr = malloc(rhssize + 1);
    strcpy(buffr, rhs);
    reverse(buffr);

    int i = 0;
    while (1 == 1) {

        tmp = 0;
        if (i < lhssize && i < rhssize) {
            tmp = get_digit_value( buffl[i]) * get_digit_value(buffr[i]) - retenue;
            // cas si depassement de un
        } else if (i < lhssize && i >= rhssize) {
            tmp = get_digit_value(buffl[i]) * retenue;
            // cas si depassement de l' autre
        } else if (i >= lhssize && i < rhssize) {
            tmp = get_digit_value(buffr[i]) * retenue;
        }

        // mettre tmp en dessous de la base
        while (tmp >= (int)base) {
          retenue++;
          tmp -= base;
        }

        // appliquer la base
        res = realloc(res, sizeof(char) * i + 2);
        res[i] = to_digit(tmp);
        tmp = 0;

        printf("I: %d, RES: %s\n", i, res);
        if (i >= lhssize - 1 && i >= rhssize - 1) {
            printf("rentrer dans le depassement\n");
            i++;
            for (int j = 0; j < count_digit(retenue, base); j++, i++) {
                printf("j: %d\n", j);
                res = realloc(res, sizeof(char) * i + 2);
                res[i] = to_digit(get_digit_in_number(retenue, base, count_digit(retenue, base) - j + 1));
            }
            return res;
        }

        i++;
    }

    return res;
}

int main() {
    /* char test[] = "91"; */
    /* char test2[] = "91"; */
    /* char *res = arithmatoy_mul(10, test, test2); */
    /* printf("RES: %s\n", res); */
    /* printf("%s\n", mul_digit(16, "e4", 'f')); */
    char *a;
    a = malloc(3);
    a = "123";
    printf("number: 123, res: %s", pow_base(a, 2));
    return 0;
}
